defmodule Buildah.Apk.Php.Pdo.Sqlite do

    alias Buildah.{Apk, Apk.Catatonit, Cmd}

    def on_container(container, options) do
        user = "user"
        {uid_, 0} = Cmd.run(
            container,
            ["id", "-u", user]
        )
        {gid_, 0} = Cmd.run(
            container,
            ["id", "-g", user]
        )
        Apk.packages_no_cache(container, [
        # Common most
            "composer",
            "git",
            "php-curl",
            "php-gd",
            "php-mbstring",
            "php-xml",
            "php-zip",
            "unzip",
        # Apk most
            "php",
            "php-ctype",
            "php-dom",
            "php-iconv",
            "php-json",
            "php-openssl",
            "php-pcntl", # Needed by symfony/web-server-bundle console server:start command.
            "php-phar",
            "php-posix", # Needed by symfony/web-server-bundle console server:start command.
            "php-session",
            "php-simplexml",
            "php-tokenizer",
            "php-xmlwriter",
        # Pdo Sqlite
            "php-pdo_sqlite"
        ], options)
        {_, 0} = Cmd.run(container, ["sh", "-c", "sed --in-place='~' -- 's#^;date.timezone =\\$#date.timezone = America/New_York#g' /etc/php*/php.ini"])
        # {_, 0} = Cmd.run(container, ["install", "-d", "~/var"])
        {_, 0} = Cmd.run(container, ["setpriv", "--reuid=#{String.trim(uid_)}", "--regid=#{String.trim(gid_)}", "--init-groups", "--", "install", "-d", "/home/#{user}/var"]) # $HOME is probably not defined in buildah, because it's defined in entrypoint. It may be found with podman.
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v php"])
        {_, 0} = Cmd.config(
            container,
            env: [
                "COMPOSER_NO_INTERACTION=1",
                "DB_TYPE=sqlite",
                "DATABASE_URL=sqlite:////home/user/var/data.db" # TODO: check $HOME and $USER (filename: ${USER}.db).
            ],
            into: IO.stream(:stdio, :line)
        )
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, ["sh", "-c", "command -v php"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "COMPOSER_NO_INTERACTION"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["printenv", "DB_TYPE"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["sh", "-c", "command -v php"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end
